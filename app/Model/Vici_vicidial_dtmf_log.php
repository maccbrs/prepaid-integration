<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vici_vicidial_dtmf_log extends Model
{

	protected $connection = 'vicidial';
    protected $table = 'vicidial_dtmf_log';
    protected $fillable = [];  

}