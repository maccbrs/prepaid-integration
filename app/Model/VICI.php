<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class VICI extends Model
{

    protected $connection = 'vicidial';
    protected $table = 'vicidial_closer_log';

    public function logs($campaign){
        $dates = mb_two_dates();
        return $this->where('call_date','>=',$dates['yesterday'])
                ->where('call_date','<=',$dates['today'])
                ->whereIn('campaign_id',$campaign)
                ->get()->toArray();
    }

    public function logs2(){
        $dates = mb_two_dates();
        return $this
            ->whereBetween('call_date', [$dates['yesterday'], $dates['today']])
            ->get()->toArray();
    }

    public function ingroups($server,$bound){


        switch ($bound) {
            case 'out':
                $table = 'vicidial_log';
                break;
            
            default:
                $table = 'vicidial_closer_log';
                break;
        }

        return DB::connection($conn)
                        ->table($table)
                        ->select('campaign_id')
                        ->groupBy('campaign_id')
                        ->lists('campaign_id');
    }

    public function allgroups(){

        $in = DB::connection('vicidial')
            ->table('vicidial_closer_log')
            ->select('campaign_id')
            ->groupBy('campaign_id')
            ->lists('campaign_id');

        $out = DB::connection('vicidial')
            ->table('vicidial_log')
            ->select('campaign_id')
            ->groupBy('campaign_id')
            ->lists('campaign_id');

        $allcampaigns = array_merge($in,$out);

        return $allcampaigns;
    }


    public static function vici_call_logs($date,$newdate,$campaign = array()){

        return Self::select('status',DB::raw('count(*) as count'))
                ->where('call_date','>=',$newdate)
                ->where('call_date','<=',$date)
                ->whereIn('campaign_id',$campaign)
                ->whereNotIn('status',['DROP','INCALL','QUEUE'])
                ->groupBy('status')
                ->orderBy('status', 'asc')
                ->get()->toArray();
    }

    public static function call_logs(){

        $calls = DB::connection('vicidial')
            ->table('vicidial_closer_log')
            ->join('vicidial_agent_log','vicidial_agent_log.lead_id','=','vicidial_closer_log.lead_id')
            ->select(
                'vicidial_closer_log.lead_id',
                'vicidial_closer_log.campaign_id',
                'vicidial_closer_log.user',
                'vicidial_closer_log.call_date',
                'vicidial_closer_log.status',
                'vicidial_closer_log.phone_number',
                'vicidial_closer_log.comments',
                'vicidial_closer_log.queue_seconds',
                'vicidial_agent_log.*'
                )
            ->take(100)
            ->get();

        return $calls;
    }


}