<?php  namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CampaignData extends Model
{
    protected $table = 'campaign_data';
    protected $fillable = ['status','campaign_id', 'lob','contents','active','pageid', 'created_at']; 



} 