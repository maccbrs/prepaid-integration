<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->call(function () {

        //     $ld = new \App\Http\Controllers\checkprepaid\loaddetailsController;
        //     $ld->check('regular',0);
        //     $ld->check('reserved',0);

        // })->everyMinute();

        // $schedule->call(function () {

        //     $ld = new \App\Http\Controllers\checkprepaid\loaddetailsController;
        //     $ld->check('regular',1);
        //     $ld->check('reserved',1);

        // })->everyFiveMinutes();

        // $schedule->call(function () {

        //     $ld = new \App\Http\Controllers\checkprepaid\loaddetailsController;
        //     $ld->check('regular',2);
        //     $ld->check('reserved',2);

        // })->everyThirtyMinutes();

        // $schedule->call(function () {

        //     $ld = new \App\Http\Controllers\checkprepaid\loaddetailsController;
        //     $ld->check('regular',3);
        //     $ld->check('reserved',3);

        // })->hourly();

        // $schedule->call(function () {

        //     $ld = new \App\Http\Controllers\checkprepaid\loaddetailsController;
        //     $ld->updateLoads();

        // })->cron('*/2 * * * *');;

        $schedule->call(function () {

            $email = new \App\Http\Controllers\EmailController;
            $email->customEmail();
          //  echo 'test';
        })->cron('30 13 * * *'); 

         $schedule->call(function () {

            $email = new \App\Http\Controllers\EmailController;
            $email->specialRequest();
          //  echo 'test';
        })->cron('0 12 24 * *'); 

        $schedule->call(function () {

            $email = new \App\Http\Controllers\EmailController;
            $email->notification();
          //  echo 'test';
        })->cron('*/1 * * * *'); 

        $schedule->call(function () {

            $load = new \App\Http\Controllers\checkprepaid\loadController;
            $load->updateLoad();

        })->cron('*/1 * * * *'); 

        $schedule->call(function () {

            $load = new \App\Http\Controllers\checkprepaid\loadController;
            $load->updateOverage();

        })->cron('*/1 * * * *'); 

        $schedule->call(function () {

            $load = new \App\Http\Controllers\checkprepaid\loadController;
            $load->complete();

        })->cron('*/2 * * * *'); 

        $schedule->call(function () {

            $load = new \App\Http\Controllers\checkprepaid\loadController;
            $load->completeDetails(0);

        })->everyThirtyMinutes(); 

        $schedule->call(function () {

            $load = new \App\Http\Controllers\checkprepaid\loadController;
            $load->completeDetails(1);

        })->hourly(); 



        $schedule->call(function () {

            $load = new \App\Http\Controllers\checkprepaid\loadController;
            $load->checkload();

        })->cron('*/1 * * * *');         

        $schedule->call(function () {

            $obj = new \App\Http\Controllers\eod\cronController;
            $obj->eodStep1();

        })->cron('1 0 * * *'); 
                

        $schedule->call(function () {
            $obj = new \App\Http\Controllers\eod\cronController;
            $obj->eodStep2();
        })->cron('*/1 * * * *');

        $schedule->call(function () {

            $obj = new \App\Http\Controllers\eod\cronController;           
            $obj->eodStep3();

        })->cron('*/1 * * * *'); 
        
        // $schedule->call(function () {

        //     $email = new \App\Http\Controllers\EmailController;
        //     $email->notification();
        //   //  echo 'test';
        // })->cron('*/1 * * * *'); 

       
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
