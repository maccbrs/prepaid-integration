<?php namespace App\Http\Models\eod;

use Illuminate\Database\Eloquent\Model;
use Auth;

class EodTester extends Model
{
   protected $connection = 'rhyolite';
   protected $table = 'eod_cron_checker';
   protected $timestamp = false;
   protected $fillable = ['send_time','triggered_time','timezone_to','timezone_to_time','timezone_from','timezone_from_time','count','notes'];

}