<?php namespace App\Http\Models\eod;

use Illuminate\Database\Eloquent\Model;

class CampaignData extends Model
{

	protected $connection = 'interface';
    protected $table = 'campaign_data';
    protected $fillable = ['status','campaign_id', 'lob','contents','active']; 



} 