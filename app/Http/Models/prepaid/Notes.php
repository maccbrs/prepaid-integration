<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Notes extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'notes';
   protected $fillable = ['content','subscriber_id','created_at','users_id'];
}