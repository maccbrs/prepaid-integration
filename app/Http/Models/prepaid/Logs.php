<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Logs extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'logs';
   protected $fillable = ['did','callid','loadid','type','msg','remaining','subscriber','created_at','updated_at'];

 }

