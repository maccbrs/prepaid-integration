<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reserved extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'reserved';
   protected $fillable = ['minutes','subscriber_id','used','remaining','status','updated_at','records'];

	public function details(){
		return $this->hasMany('App\Http\Models\prepaid\Reserved_details','subscriber_id','subscriber_id')->with(['closer']);
	}   

}