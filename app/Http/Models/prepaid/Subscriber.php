<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Subscriber extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'subscribers';
   protected $fillable = ['client','email','prefix'];

	// public function scopeWithactiveload($query)
	// {
 //        $dt = Carbon::now();
 //        $now = $dt->format('Y-m-d');
 //        return $query->where('startdate', '<=', $now)->where('enddate', '>=', $now)->where('status','=',1);
	// }   

	public function loads(){
		return $this->hasMany('App\Http\Models\prepaid\Load', 'subscriber_id');
	}
	public function reserves(){
		return $this->hasOne('App\Http\Models\prepaid\Reserved', 'subscriber_id');
	} 

	public function reserved(){
		return $this->hasOne('App\Http\Models\prepaid\Reserved', 'subscriber_id');
	} 

	public function notes(){ 
		return $this->hasMany('App\Http\Models\prepaid\Notes', 'subscriber_id');
	} 

	public function connected(){
		return $this->hasOne('App\Http\Models\prepaid\Connector', 'subscriber_id');
	}

	public function active(){
		return $this->hasOne('App\Http\Models\prepaid\Load', 'subscriber_id')->with(['details'])->active();
	}

	public function emailers(){
		return $this->hasOne('App\Http\Models\prepaid\Emailer', 'subscriber_id');
	} 

	public function outboundloads(){
		return $this->hasMany('App\Http\Models\prepaid\OutboundLoad', 'subscriber_id');
	}
			 	   
}