<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Vici_closer extends Model
{
    protected $connection = '192.168.200.132';
    protected $table = 'vicidial_closer_log';

   public function agent(){
   		return $this->hasOne('App\Http\Models\prepaid\Vici_agent','uniqueid','uniqueid');
   } 

}