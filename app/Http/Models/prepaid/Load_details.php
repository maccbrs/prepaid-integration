<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Load_details extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'load_details';
   protected $fillable = ['load_id','uniqueid','phoneno','callerid'];


   public function closer(){
   		return $this->hasOne('App\Http\Models\prepaid\Vici_closer','uniqueid','uniqueid')->whereNotIn('status', ["DROP","HU","Test"])->with(['agent']);
   }

   public function closer2(){
   		return $this->hasOne('App\Http\Models\prepaid\Vici_closer','uniqueid','uniqueid')->with(['agent']);
   }

}