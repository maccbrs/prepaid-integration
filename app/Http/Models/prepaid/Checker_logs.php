<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Checker_logs extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'checker_logs';
   protected $fillable = ['type','count','checktype','created_at','updated_at'];

   
}