<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class LoadUpdater extends Model
{
	
   protected $connection = 'prepaid';
   protected $table = 'load_updater';
   protected $fillable = ['title','content','created_at','updated_at'];



}