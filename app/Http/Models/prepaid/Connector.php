<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Connector extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'connectors';
   protected $fillable = ['subscriber_id','did','status'];

	public function subscriber(){
		return $this->hasOne('App\Http\Models\prepaid\Subscriber','id','subscriber_id');
	} 

	public function activeload(){
        $dt = Carbon::now();
        $now = $dt->format('Y-m-d');	
		return $this->hasOne('App\Http\Models\prepaid\Load','subscriber_id','subscriber_id')->where('startdate', '<=', $now)->where('enddate', '>=', $now)->where('status','=',1);
	}  	    
}