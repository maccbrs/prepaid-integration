<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class OutboundLoad extends Model
{
	protected $connection = 'prepaid';
	protected $table = 'outbound_loads';
	protected $fillable = ['startdate','enddate','minutes','subscriber_id','used','remaining','status','created_at','updated_at'];

	public function scopeActive($query)
	{
        $dt = Carbon::now();
        $now = $dt->format('Y-m-d');
        return $query->where('startdate', '<=', $now)->where('enddate', '>=', $now)->where('status','=',1);
	}

	public function details(){
		return $this->hasMany('App\Http\Models\prepaid\OutboundLoadDetails','outbound_load_id')->with(['call_details']);
	}

}