<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Notifications extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'notifications';
   protected $fillable = ['load_id','type','content','to','cc','created_at','updated_at'];

	   
}