<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class Load extends Model
{
	protected $connection = 'prepaid';
   protected $table = 'loads';
   protected $fillable = ['startdate','enddate','minutes','subscriber_id','used','remaining','status','created_at','updated_at'];


	public function scopeActive($query)
	{
        $dt = Carbon::now();
        $now = $dt->format('Y-m-d');
        return $query->where('startdate', '<=', $now)->where('enddate', '>=', $now)->where('status','=',1);
	}

	public function scopeConsumed($q){
        $dt = Carbon::now();
        $now = $dt->format('Y-m-d');
        return $q->where('startdate', '<=', $now)->where('enddate', '>=', $now)->where('status','=',0);	
	}
	
	public function details(){
		return $this->hasMany('App\Http\Models\prepaid\Load_details','load_id')->with(['closer']);
	}

	public function details2(){
		return $this->hasMany('App\Http\Models\prepaid\Load_details','load_id')->with(['closer2']);
	}

	public function subscriber(){
		return $this->belongsTo('App\Http\Models\prepaid\Subscriber','subscriber_id')->with(['reserved']);
	}

	public function emails(){
		return $this->belongsTo('App\Http\Models\prepaid\Emailer','subscriber_id','subscriber_id');
	}

	public function notifications(){
		return $this->hasMany('App\Http\Models\prepaid\Notifications','load_id');
	}

}  