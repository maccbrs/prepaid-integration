<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reserved_details extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'reserved_details';
   protected $fillable = ['subscriber_id','uniqueid','updated_at'];

   public function closer(){
   		return $this->hasOne('App\Http\Models\prepaid\Vici_closer','uniqueid','uniqueid')->whereNotIn('status', ["DROP","HU","Test"])->with(['agent']);
   }
   
}