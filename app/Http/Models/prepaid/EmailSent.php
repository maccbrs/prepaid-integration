<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;


class EmailSent extends Model
{
	protected $connection = 'prepaid';
   protected $table = 'emailsent';
   protected $fillable = ['load_id','type','message','to','created_at','updated_at'];



}  