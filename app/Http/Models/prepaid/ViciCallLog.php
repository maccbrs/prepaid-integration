<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ViciCallLog extends Model
{
    protected $connection = '192.168.200.132'; 
    protected $fillable = ['extension', 'number_dialed','caller_code','start_time','start_epoch','end_time','end_epoch','length_in_sec','length_in_min'];
    protected $table = 'call_log';

}




