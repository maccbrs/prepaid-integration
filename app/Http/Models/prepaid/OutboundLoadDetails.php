<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;

class OutboundLoadDetails extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'outbound_load_details';
   protected $fillable = ['outbound_load_id','is_reserved','extension','startepoch','created_at','updated_at'];

	public function scopeLast5min($query)
	{
		$x = $this->orderBy('created_at','desc')->first();
        $y = Carbon::parse($x->created_at)->modify('-5 min');
        $t = $y->format('y-m-d H:i:s');
        return $query->where('created_at', '>=', $t);
	}

   public function call_details(){
     return $this->hasOne('App\Http\Models\prepaid\ViciCallLog','uniqueid','uniqueid');
   }


}

