<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ReservedDetails extends Model
{

   protected $connection = 'jade';
   protected $table = 'reserved_details';
   protected $fillable = ['subscriber_id','phoneno','callerid','transaction_id','checked','duration','uniqueid'];

}