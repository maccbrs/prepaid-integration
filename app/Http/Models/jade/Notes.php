<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Notes extends Model
{

   protected $connection = 'jade';
   protected $table = 'notes';
   protected $fillable = ['users_id','subscriber_id','content'];

}