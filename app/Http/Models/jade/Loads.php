<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Loads extends Model
{

   protected $connection = 'jade';
   protected $table = 'loads';
   protected $fillable = ['subscriber_id','used','minutes','remaining','status','startdate','enddate','month','year','breakdown'];

   public function subscriber(){
      return $this->hasOne('App\Http\Models\jade\Subscribers','id','subscriber_id');
   }    

}