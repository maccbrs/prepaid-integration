<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class LoadDetails extends Model
{

   protected $connection = 'jade';
   protected $table = 'load_details';
   protected $fillable = ['transaction_id','did','verified'];

}