<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reserved extends Model
{

   protected $connection = 'jade';
   protected $table = 'reserved';
   protected $fillable = ['subscriber_id','minutes','used','remaining','status','breakdown'];

}