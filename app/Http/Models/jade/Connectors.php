<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Connectors extends Model 
{

   protected $connection = 'jade';
   protected $table = 'connectors';
   protected $fillable = ['subscriber_id','did','status','load_id','load_status','reserved_id','reserved_status','account_status'];

}