<?php namespace App\Http\Models\jade;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Subscribers extends Model
{

   protected $connection = 'jade';
   protected $table = 'subscribers';
   protected $fillable = ['client','email','alias','status'];

}