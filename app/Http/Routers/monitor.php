<?php 

$this->group(['prefix' => 'calls'],function(){
	$this->get('/',['as' => 'monitor.calls.index','uses' => 'callsController@calls']);
});

$this->group(['prefix' => 'updater'],function(){
	$this->get('/check/{type}/{min}',['as' => 'monitor.updater.check','uses' => 'updaterController@check']);
});