<?php 

$this->group(['prefix' => 'cron'],function(){
	$this->get('/tester',['as' => 'eod.cron.tester','uses' => 'cronController@tester']);
	$this->get('/convert',['as' => 'eod.cron.convert','uses' => 'cronController@convertSendTime']);
	$this->get('/checktimer',['as' => 'eod.cron.checktimer','uses' => 'cronController@checkTimer']);
	$this->get('/emailer',['as' => 'eod.cron.emailer','uses' => 'cronController@emailer']);
});