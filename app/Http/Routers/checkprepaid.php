<?php 

$this->group(['prefix' => 'loaddetails'],function(){
	$this->get('/index',['as' => 'checkprepaid.loaddetails.index','uses' => 'loaddetailsController@index']);
	$this->get('/emailer',['as' => 'checkprepaid.loaddetails.emailer','uses' => 'loaddetailsController@emailer']);

});

// checkprepaid/load/simulate-call
// checkprepaid/load/checkload
$this->group(['prefix' => 'load'],function(){
	$this->get('/simulate-call',['as' => 'checkprepaid.loaddetails.simulate-call','uses' => 'loadController@simulateCall']);
	$this->get('/test2/{id}',['as' => 'checkprepaid.loaddetails.test2','uses' => 'loadController@test2']);
	$this->get('/checkload',['as' => 'checkprepaid.loaddetails.checkload','uses' => 'loadController@checkload']);
	$this->get('/update-overage',['as' => 'checkprepaid.loaddetails.update-overage','uses' => 'loadController@updateOverage']);
	$this->get('/complete-details/{n}',['as' => 'checkprepaid.loaddetails.complete-details','uses' => 'loadController@completeDetails']);
	$this->get('/updateload',['as' => 'checkprepaid.loaddetails.updateload','uses' => 'loadController@updateload']);
});