<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\logsController as Logs;
use Auth;

class homeController extends Controller
{

	public function index(Request $r){

		 $did = $r->e;
		 $transid = $r->d;

		return $this->call($did,$transid);

	}

	public function call($did,$transid){

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Hungups = new \App\Http\Models\jade\Hungups;

		$msg = '';

		$a = $Connectors->where('did',$did)->first();


		if($a->account_status == 'open'):

			$CallDetails = new \App\Http\Models\jade\CallDetails;

				if($a->load_status):

					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->load_id,
					  'load_type' => 1,
					  'subscriber_id' => $a->subscriber_id
					]);

				elseif($a->reserved_status):

					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->reserved_id,
					  'load_type' => 2,
					  'subscriber_id' => $a->subscriber_id
					]);
					
				else:
					$msg = 'no load';
				endif;

				return 1;

		endif;

		if($a->account_status == 'suspended'):

			$msg = 'did '.$did.' is suspended > transaction_id >> '.$transid;
			$Hungups->create([
				'did' => $did,
				'message' => $msg
			]);
			return 0;

		endif;

		if($a):
			if($a->status):

				$CallDetails = new \App\Http\Models\jade\CallDetails;
				if($a->load_status):

					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->load_id,
					  'load_type' => 1,
					  'subscriber_id' => $a->subscriber_id
					]);
					return 1;

				elseif($a->reserved_status):


					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->reserved_id,
					  'load_type' => 2,
					  'subscriber_id' => $a->subscriber_id
					]);
					return 1;

				else:

					$msg = 'no load';

				endif;
			else:
				$msg = 'subscriber disabled';
			endif;
		else:
			
			$msg = 'did '.$did.' not registered > transaction_id >> '.$transid;

		endif;

		$Hungups->create([
			'did' => $did,
			'message' => $msg
		]);

		return 0;

	} 	

}