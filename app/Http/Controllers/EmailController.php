<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\jade\Loads;
use Mail;
use App\Http\Models\jade\Subscribers;
use App\Http\Models\jade\Reserved;
class EmailController extends Controller
{
	public function notification(){
	//return view('emails.testemail');
		$month = date('n');
		$year = date('Y');

		$actives = Loads::where('month', $month)
		->where('year', $year)
		->whereNotIn('subscriber_id', [11,33])
		->with('subscriber')
		->get();

		//$emails = ['hazel.osias@magellan-solutions.com', 'glenn.casaljay@magellan-solutions.com', 'jovielyn.bauyon@magellan-solutions.com'];
		$cc = ['eugene.guevara@magellan-solutions.com', 'maristela.bernardino@magellan-solutions.com', 'teamomnitrix@magellan-solutions.com'];
		$bcc = ['webdev@magellan-solutions.com'];
		foreach($actives as $active){
			
			$emails = $active->subscriber->email;
			$checker =  $active->minutes * 0.50;
			$checker1 =  $active->minutes * 0.80;

			$reserve = Reserved::where('subscriber_id', $active->subscriber_id)->first();
			$reserve_checker = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.50;
			$reserve_checker1 = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.80;
			$reserve_checker2 = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.90;

			if($active->used >= $checker && $active->used < $checker1){

				if($active->email_status == 1 || $active->email_status == 2 || $active->email_status == 3){
					//echo '50% has been sent already'.'<br>';

				}
				else{
					$update = Loads::where('subscriber_id', $active->subscriber_id)->update(['email_status' => 1]);
					if($update){

						Mail::send('emails.email_notification', 
							[	'emails' => $emails,
							'used' => ($active->used >= $checker && $active->used < $checker1) ? "50%" : "",
							'remaining' => $active->remaining,
							'subscriber' => $active->subscriber->company,
							'overage_used' => 
							($active->used >= $active->minutes) ?
							($reserve->used >= $reserve_checker && $reserve->used < $reserve_checker1) ? "50%" : ""
							: ($reserve == null) ? "0%" : round($reserve->used / ($reserve->minutes / 100)). "%",
							'overage_remaining' => ($reserve == null) ? "0" : $reserve->remaining,
							'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
							'expiration_date' => date("m-d-Y", strtotime($active->enddate))
						], 
						function ($m) use ($emails, $cc, $bcc)  {
							$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
							$m->to($emails);
							$m->subject('Minutes Report');
							$m->cc($cc);
							$m->bcc($bcc);
						});
						//echo 'send successfully ';
					}
				//	echo $active->subscriber_id. '| 50 na';
				//	echo '<br>';
				}
			}

			if($active->used >= $checker1 && $active->used < $active->minutes){
				if($active->email_status == 2){
				//	echo '80% update has been sent already'.'<br>';
				}
				else{
					$update = Loads::where('subscriber_id', $active->subscriber_id)->update(['email_status' => 2]);
					if($update){
						Mail::send('emails.email_notification', 
							[	'emails' => $emails,
							'used' => ($active->used >= $checker1 && $active->used < $active->minutes) ? '80%' : "",
							'remaining' => $active->remaining,
							'subscriber' => $active->subscriber->company,
							'overage_used' => 
							($active->used >= $active->minutes) ?
							($reserve->used >= $reserve_checker1 && $reserve->used < $reserve->minutes) ? "80%" : ""
							: ($reserve == null) ? "0%" : round($reserve->used / ($reserve->minutes / 100)). "%",
							'overage_remaining' => ($reserve == null) ? "0" : $reserve->remaining,
							'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
							'expiration_date' => date("m-d-Y", strtotime($active->enddate))
						],  
						function ($m) use ($emails, $cc, $bcc)  {
							$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
							$m->to($emails);
							$m->subject('Minutes Report');
							$m->cc($cc);
							$m->bcc($bcc);
						});
					//	echo 'send successfully ';
					//	echo $active->subscriber_id.'| 80 na';
					//	echo '<br>';
					}
				}
			}

			if($active->used >= $active->minutes){

				if($active->email_status == 3 && $reserve != null){
					//echo '100% has been sent already'.'<br>';
					if($reserve->used >= $reserve_checker && $reserve->used < $reserve_checker1){
						//echo 'Overage reached 50%'.'<br>';
						// || $reserve->email_status == 2 || $reserve->email_status == 3 || $reserve->email_status == 4
						if($reserve->email_status == 1){
							//echo 'Overage has reached 50%, mail has been sent already'.'<br>';
						}
						else{
							$update = Reserved::where('subscriber_id', $reserve->subscriber_id)->update(['email_status' => 1]);
							if($update){
								Mail::send('emails.email_notification', 
									[	'emails' => $emails,
									'used' => ($active->used >= $active->minutes) ? '100%' : "",
									'remaining' => $active->remaining,
									'subscriber' => $active->subscriber->company,
									'overage_used' => ($reserve->used >= $reserve_checker && $reserve->used < $reserve_checker1) ? "50%" : "",
									'overage_remaining' => $reserve->remaining,
									'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
									'expiration_date' => date("m-d-Y", strtotime($active->enddate))
								],  
								function ($m) use ($emails, $cc, $bcc)  {
									$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
									$m->to($emails);
									$m->subject('Minutes Report');
									$m->cc($cc);
									$m->bcc($bcc);
								});
								//echo 'Overage has reached 50%, mail sent successfully';
								//echo '<br>';
							}
						}
					}

					if($reserve->used >= $reserve_checker1 && $reserve->used < $reserve_checker2){

						if($reserve->email_status == 2){
							//echo 'Overage has reached 80%, mail has been sent already'.'<br>';
						}
						else{
							$update = Reserved::where('subscriber_id', $reserve->subscriber_id)->update(['email_status' => 2]);
							if($update){
								Mail::send('emails.email_notification', 
									[	'emails' => $emails,
									'used' => ($active->used >= $active->minutes) ? '100%' : "",
									'remaining' => $active->remaining,
									'subscriber' => $active->subscriber->company,
									'overage_used' => ($reserve->used >= $reserve_checker1 && $reserve->used < $reserve_checker2) ? "80%" : "",
									'overage_remaining' => $reserve->remaining,
									'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
									'expiration_date' => date("m-d-Y", strtotime($active->enddate))
								],  
								function ($m) use ($emails, $cc, $bcc)  {
									$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
									$m->to($emails);
									$m->subject('Minutes Report');
									$m->cc($cc);
									$m->bcc($bcc);

								});
								//echo 'Overage has reached 80%, mail sent successfully';
								//echo '<br>';
							}
						}
					}

						// 90% RESERVE EMAIL NOTIF
					if($reserve->used >= $reserve_checker2 && $reserve->used < $reserve->minutes){

						if($reserve->email_status == 4){
							//echo 'Overage has reached 90%, mail has been sent already'.'<br>';
						}
						else{
							$update = Reserved::where('subscriber_id', $reserve->subscriber_id)->update(['email_status' => 4]);
							if($update){
								Mail::send('emails.email_notification', 
									[	'emails' => $emails,
									'used' => ($active->used >= $active->minutes) ? '100%' : "",
									'remaining' => $active->remaining,
									'subscriber' => $active->subscriber->company,
									'overage_used' => ($reserve->used >= $reserve_checker2 && $reserve->used < $reserve->minutes) ? "90%" : "",
									'overage_remaining' => $reserve->remaining,
									'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
									'expiration_date' => date("m-d-Y", strtotime($active->enddate))
								],  
								function ($m) use ($emails, $cc, $bcc)  {
									$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
									$m->to($emails);
									$m->subject('Minutes Report');
									$m->cc($cc);
									$m->bcc($bcc);
								});
								// echo 'Overage has reached 90%, mail sent successfully';
								// echo '<br>';
							}
						}
					}

					if($reserve->used >= $reserve->minutes){

						if($reserve->email_status == 3){
							//echo 'Overage has reached 100%, mail has been sent already'.'<br>';

						}else{
							$update = Reserved::where('subscriber_id', $reserve->subscriber_id)->update(['email_status' => 3]);
							if($update){
								Mail::send('emails.email_notification', 
									[	'emails' => $emails,
									'used' => ($active->used >= $active->minutes) ? '100%' : "",
									'remaining' => $active->remaining,
									'subscriber' => $active->subscriber->company,
									'overage_used' => ($reserve->used >= $reserve->minutes) ? "100%" : "",
									'overage_remaining' => $reserve->remaining,
									'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
									'expiration_date' => date("m-d-Y", strtotime($active->enddate))
								],  
								function ($m) use ($emails, $cc, $bcc)  {
									$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
									$m->to($emails);
									$m->subject('Minutes Report');
									$m->cc($cc);
									$m->bcc($bcc);
								});
								//echo 'Overage has reached 100%, mail sent successfully';
								//echo '<br>';
							}
						}
					}

				}
				else{

					if ($active->email_status == 3 && $reserve == null){
					//echo 'bawal'.'<br>';
					}else{

					$update = Loads::where('subscriber_id', $active->subscriber_id)->update(['email_status' => 3]);
					if($update){
						Mail::send('emails.email_notification', 
							[	'emails' => $emails,
							'used' => ($active->used >= $active->minutes) ? '100%' : "",
							'remaining' => $active->remaining,
							'subscriber' => $active->subscriber->company,
							'overage_used' => 
							($active->used >= $active->minutes) ?
							($reserve == null) ? "0%" :
							($reserve->used >= $reserve->minutes) ? "100%" : round($reserve->used / ($reserve->minutes / 100)). "%" : "",
							'overage_remaining' => ($reserve == null) ? "0" : $reserve->remaining,
							'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
							'expiration_date' => date("m-d-Y", strtotime($active->enddate))
						],  
						function ($m) use ($emails, $cc, $bcc)  {
							$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
							$m->to($emails);
							$m->subject('Minutes Report');
							$m->cc($cc);
							$m->bcc($bcc);
						});
						//echo 'send successfully ';
						//echo $active->subscriber_id.'| 100 na';
						//echo '<br>';
						}

					}
				}
			}
		} //endforeach subscrition

	}

	public function customEmail(){

		$month = date('n');
		$year = date('Y');

		$active = Loads::where('month', $month)
		->where('year', $year)
		->where('subscriber_id', 35)
		->with('subscriber')
		->first();

		$emails = $active->subscriber->email;
		$checker =  $active->minutes * 0.50;
		$checker1 =  $active->minutes * 0.80;

		$reserve = Reserved::where('subscriber_id', 35)->first();
		$reserve_checker = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.50;
		$reserve_checker1 = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.80;
		$reserve_checker2 = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.90;

		//$emails = ['michael.luzano@magellan-solutions.com'];
		// $cc = ['webdev@magellan-solutions.com'];
		// $bcc = ['webdev@magellan-solutions.com'];
		$cc = ['eugene.guevara@magellan-solutions.com', 'maristela.bernardino@magellan-solutions.com', 'teamomnitrix@magellan-solutions.com'];
		$bcc = ['webdev@magellan-solutions.com'];

		Mail::send('emails.email_notification', 
							[	'emails' => $emails,
							'used' => round($active->used / ($active->minutes / 100)). "%",
							'remaining' => $active->remaining,
							'subscriber' => $active->subscriber->company,
							'overage_used' => round($reserve->used / ($reserve->minutes / 100)). "%",
							'overage_remaining' => ($reserve == null) ? "0" : $reserve->remaining,
							'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
							'expiration_date' => date("m-d-Y", strtotime($active->enddate))
						],  
						function ($m) use ($emails, $cc, $bcc)  {
							$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
							$m->to($emails);
							$m->subject('Minutes Report');
							$m->cc($cc);
							$m->bcc($bcc);
						});
	

	}

	public function specialRequest(){

		$month = date('n');
		$year = date('Y');

		$active = Loads::where('month', $month)
		->where('year', $year)
		->where('subscriber_id', 38)
		->with('subscriber')
		->first();

		$emails = $active->subscriber->email;
		$checker =  $active->minutes * 0.50;
		$checker1 =  $active->minutes * 0.80;

		$reserve = Reserved::where('subscriber_id', 38)->first();
		$reserve_checker = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.50;
		$reserve_checker1 = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.80;
		$reserve_checker2 = (!empty($reserve->minutes) ? $reserve->minutes : 0) * 0.90;

		//$emails = ['michael.luzano@magellan-solutions.com'];
		// $cc = ['webdev@magellan-solutions.com'];
		// $bcc = ['webdev@magellan-solutions.com'];
		$cc = ['eugene.guevara@magellan-solutions.com', 'maristela.bernardino@magellan-solutions.com', 'teamomnitrix@magellan-solutions.com'];
		$bcc = ['webdev@magellan-solutions.com'];

		Mail::send('emails.email_notification', 
							[	'emails' => $emails,
							'used' => round($active->used / ($active->minutes / 100)). "%",
							'remaining' => $active->remaining,
							'subscriber' => $active->subscriber->company,
							'overage_used' => round($reserve->used / ($reserve->minutes / 100)). "%",
							'overage_remaining' => ($reserve == null) ? "0" : $reserve->remaining,
							'subscription_date' => date("m-d-Y", strtotime($active->startdate)),
							'expiration_date' => date("m-d-Y", strtotime($active->enddate))
						],  
						function ($m) use ($emails, $cc, $bcc)  {
							$m->from('teamomnitrix@magellan-solutions.com', 'Reports');
							$m->to($emails);
							$m->subject('Minutes Report');
							$m->cc($cc);
							$m->bcc($bcc);
						});

	}
}

