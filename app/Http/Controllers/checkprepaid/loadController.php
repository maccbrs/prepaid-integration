<?php namespace App\Http\Controllers\checkprepaid;

use App\Http\Controllers\Controller;
use App\Http\Controllers\logsController as Logs;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Mail\remaining;
use Auth;
use Mail;
//use Illuminate\Support\Facades\Mail;

class loadController extends Controller
{

	public function simulateCall(){

		$LoadDetails = new \App\Http\Models\jade\LoadDetails;
		$item = $LoadDetails->where('visited',0)->first();
		if($item):

			$this->test1($item->callid,$item->did);

			$item->visited = 1;
			$item->save();
		endif;


	}

	public function test2($id){

		$this->completeDetails2($id);
	}

	public function test1($did,$transid){

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Hungups = new \App\Http\Models\jade\Hungups;

		$msg = '';

		$a = $Connectors->where('did',$did)->first();
		if($a):
			if($a->status):

				$CallDetails = new \App\Http\Models\jade\CallDetails;
				if($a->load_status):

					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->load_id,
					  'load_type' => 1,
					  'subscriber_id' => $a->subscriber_id
					]);
					return 1;

				elseif($a->reserved_status):


					$CallDetails->create([
					  'transaction_id' => $transid,
					  'did' => $did,
					  'load_id' => $a->reserved_id,
					  'load_type' => 2,
					  'subscriber_id' => $a->subscriber_id
					]);
					return 2;

				else:

					$msg = 'no load';

				endif;
			else:
				$msg = 'subscriber disabled';
			endif;
		else:
			$msg = 'did '.$did.' not registered';
		endif;

		$Hungups->create([
			'did' => $did,
			'message' => $msg
		]);

		return 3;

	} 

	public function checkload(){

		$year = date('Y');
		$month = date('m');

		$Connectors = new \App\Http\Models\jade\Connectors;
		$Loads = new \App\Http\Models\jade\Loads;
		$Reserved = new \App\Http\Models\jade\Reserved;
		$items = $Connectors->groupBy('subscriber_id')->get();

		foreach($items as $item):
			$load_id = $load_status = $reserved_status = $reserved_id = 0;

			$load = $Loads->where('subscriber_id',$item->subscriber_id)->where('month',$month)->where('year',$year)->where('remaining','>',0)->where('status',1)->first();
			if($load):
				$load_id = $load->id;
				$load_status = 1;
			else:
				$load_status = 0;
			endif;

			$res = $Reserved->where('subscriber_id',$item->subscriber_id)->where('remaining','>',0)->where('status',1)->first();
			if($res):
				$reserved_id = $res->id;
				$reserved_status = 1;
			else:
				$reserved_status = 0;
			endif;
			
			$Connectors->where('subscriber_id',$item->subscriber_id)->update([
					'load_id' => $load_id,
					'load_status' => $load_status,
					'reserved_status' => $reserved_status,
					'reserved_id' => $reserved_id
				]);

		endforeach; 

	}	

	public function complete(){

		$DboCalldetailViw = new \App\Http\Models\nexus\DboCalldetailViw; 
		$CallDetails = new \App\Http\Models\jade\CallDetails;
		$items = $CallDetails->where('check_count','<',50)->where('is_complete',0)->whereNotNull('transaction_id')->get();
		foreach($items as $item):
			$call = $DboCalldetailViw->where('CallId','like', $item->transaction_id.'%')->first();
			if($call):
	 			$item->transaction_id = $call->CallId;
	 			$item->durations = roundsix($call->CallDurationSeconds) - round($call->tExternal/1000,2);
	 			$item->actual_durations = $call->CallDurationSeconds;
	 			$item->user = $call->LocalUserId;
	 			$item->campaign_id = $call->AssignedWorkGroup;
				$item->is_complete = 1;
			endif;
			$item->check_count++;
			$item->save();
		endforeach;
	}

	public function completeDetails($verified){ 


		$DboCalldetailViw = new \App\Http\Models\nexus\DboCalldetailViw;  
		$CallDetails = new \App\Http\Models\jade\CallDetails;

	 		$items = $CallDetails->where('verified',$verified)->where('is_complete',1)->whereNotNull('transaction_id')->get();
	 		
	 		if($items->count()):

	 			$callid = [];
	 			foreach($items as $item):
	 				$callid[] = $item->transaction_id;
	 			endforeach;

	 			$calls = $DboCalldetailViw->whereIn('CallId',$callid)->with(['wrapup'])->get();
	 		
	 			if($calls->count()):

	 				$data = $actual_durations = [];
	 				

	 				foreach($calls as $call):
	 					if($call->wrapup == 'Test Call'):
	 						$data[$call->CallId] = 0;
		 				elseif(empty($call->wrapup)):
		 					$data[$call->CallId] = 0;
	 					else:
	 						$data[$call->CallId] = roundsix($call->CallDurationSeconds);
	 					endif;
	 					$actual_durations[$call->CallId] = $call->CallDurationSeconds;
	 				endforeach;

		 			foreach($items as $item):
		 				$item->durations = (isset($data[$item->transaction_id])?$data[$item->transaction_id]:0);
		 				$item->actual_durations = (isset($actual_durations[$item->transaction_id])?$actual_durations[$item->transaction_id]:0);
		 				$item->verified = $verified + 1;
		 				$item->save();
		 			endforeach;		 				
	 			endif;

	 		endif;

		return 1;
	}

	public function updateLoad(){

			$Connectors = new \App\Http\Models\jade\Connectors;
			$dids = $Connectors->select('load_id as id')->where('status',1)->where('load_id','!=',0)->groupBy('load_id')->get();
			$Loads = new \App\Http\Models\jade\Loads;
			
			foreach($dids as $did):

				$CallDetails = new \App\Http\Models\jade\CallDetails;
				$items = $CallDetails->where('load_id',$did->id)->where('load_type',1)->where('user','!=','-')->get();
				$load = $Loads->find($did->id);
				$total = 0;
				if($items->count()):
					foreach($items as $item):
						$total += $item->durations;
					endforeach;
				endif;

				$load->remaining = $load->minutes - round($total/60,2);
				$load->used = round($total/60,2);
				//pre($load->toArray());
				$load->save();

			endforeach;


	}

	public function updateOverage(){

			$Connectors = new \App\Http\Models\jade\Connectors;
			$dids = $Connectors->select('reserved_id as id')->where('status',1)->where('reserved_id','!=',0)->groupBy('reserved_id')->get();
			 $Reserved = new \App\Http\Models\jade\Reserved;
			
			foreach($dids as $did):

				$CallDetails = new \App\Http\Models\jade\CallDetails;
				$items = $CallDetails->where('load_id',$did->id)->where('load_type',2)->where('user','!=','-')->get();
				$reserved = $Reserved->find($did->id);
				$total = 0;
				if($items->count()):
					foreach($items as $item):
						$total += $item->durations;
					endforeach;
				endif;

				$reserved->remaining = $reserved->minutes - round($total/60,2);
				$reserved->used = round($total/60,2);
				//pre($reserved->toArray());
				$reserved->save();

			endforeach;


	}


	public function completeDetails2($id){ 


		$DboCalldetailViw = new \App\Http\Models\nexus\DboCalldetailViw;  
		$CallDetails = new \App\Http\Models\jade\CallDetails;

		$call = $DboCalldetailViw->where('CallId',$id)->first();
		pre($call);

		switch($verified):
		 	case 0:

		 		$item = $CallDetails->where('transaction_id','3001552892')->whereNotNull('transaction_id')->first();

		 		if($item):
		 			//pre($item);
			 		$call = $DboCalldetailViw->where('CallId','like', '3001552905%')->first();
			 		pre($call);
			 		if($call):
			 			$item->transaction_id = $call->CallId;

			 			$item->durations = roundsix($call->CallDurationSeconds) - round($call->tExternal/1000,2);
			 			$item->actual_durations = $call->CallDurationSeconds;
			 			$item->user = $call->LocalUserId;
			 			$item->campaign_id = $call->AssignedWorkGroup;
			 		endif;	 

		 			$item->verified = 1;
		 			$item->save();	
		 			pre($item);
		 		endif;


		 		break;
		 	case 1:
		 	case 2:
		 	case 3:
		 	case 4:

		 		$items = $CallDetails->where('verified',$verified)->whereNotNull('transaction_id')->get();
		 		
		 		if($items->count()):

		 			$callid = [];
		 			foreach($items as $item):
		 				$callid[] = $item->transaction_id;
		 			endforeach;
		 			//pre($callid);
		 			$calls = $DboCalldetailViw->whereIn('CallId',$callid)->with(['wrapup'])->get();
		 			//pre($calls->count());
		 			if($calls->count()):

		 				$data = $actual_durations = [];
		 				

		 				foreach($calls as $call):
		 					if($call->wrapup == 'Test Call'):
		 						$data[$call->CallId] = 0;
		 					elseif(count($call->wrapup) == 0):
		 						$data[$call->CallId] = 0;
		 					else:
		 						$data[$call->CallId] = roundsix($call->CallDurationSeconds);
		 					endif;
		 					$actual_durations[$call->CallId] = $call->CallDurationSeconds;
		 				endforeach;

			 			foreach($items as $item):
			 				$item->durations = (isset($data[$item->transaction_id])?$data[$item->transaction_id]:0);
			 				$item->actual_durations = (isset($actual_durations[$item->transaction_id])?$actual_durations[$item->transaction_id]:0);
			 				$item->verified = $verified + 1;
			 				$item->save();
			 			endforeach;		 				
		 			endif;

		 		endif;

		 		break;		 		
		endswitch;

		return 1;
	}

}