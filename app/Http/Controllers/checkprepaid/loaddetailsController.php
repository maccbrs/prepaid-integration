<?php namespace App\Http\Controllers\checkprepaid;

use App\Http\Controllers\Controller;
use App\Http\Controllers\logsController as Logs;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Mail\remaining;
use Auth;
use Mail;
//use Illuminate\Support\Facades\Mail;

class loaddetailsController extends Controller
{

	public function index(){

		$emails = ['marlon@mailinator.com','marlonbbernal@yahoo.com','marc.briones@magellan-solutions.com'];
        Mail::send('emails.remaining', ['emails' => $emails], function ($m) use ($emails)  {
            $m->from('webdev_report@magellan-solutions.com', 'Magellan Report');
            $m->to($emails);
            $m->subject('test emailer');
        });
		//$this->testTransactionIdconsistency();

	} 


	public function testTransactionIdconsistency(){
		$CallDetail = new \App\Http\Models\nexus\DboCalldetailViw;
		$LoadDetails = new \App\Http\Models\jade\LoadDetails;

		$item = $LoadDetails->where('verified',0)->first();
		if($item):

			if($item->transaction_id):
				$call = $CallDetail->where('CallId','like',$item->transaction_id.'%')->first();
				if($call):
					$item->transaction_id = $call->CallId;
				else:
					$item->transaction_id = 'cannot find on cic';
				endif;
			else:
				$item->transaction_id = 'null transaction_id';
			endif;

			$item->verified = 1;
			$item->save();
		endif;		
	}

	public function check($type = 'regular',$count = 1){

		$logs = new Logs;
		$CallDetail = new \App\Http\Models\nexus\DboCalldetailViw; 
		$logarr = [
			'type' => $type,
			'count' => 0,
			'checktype' => $count
		];

		if($type == 'regular'):
			$Obj = new \App\Http\Models\prepaid\Load_details;
		else:
			$Obj = new \App\Http\Models\prepaid\Reserved_details;
		endif;

		switch($count):
		 	case 0:
		 		$item = $Obj->where('checked',0)->whereNotNull('transaction_id')->first();
		 		$call = $CallDetail->where('CallId','like','1001259200%')->first();
		 		if($call):
		 			$item->uniqueid = $call->CallId;
		 			$item->talk_sec = $call->CallDurationSeconds;
		 			$item->checked = 1;
		 			$item->save();
		 			$logarr['count'] = 1;
		 		endif;
		 		break;
		 	case 1:
		 	case 2:
		 	case 3:
		 		$items = $Obj->where('checked',$count)->whereNotNull('transaction_id')->get();

		 		if($items->count()):
		 			$callid = [];
		 			$logarr['count'] = $items->count();
		 			foreach($items as $item):
		 				$callid[] = $item->uniqueid;
		 			endforeach;
		 			$calls = $CallDetail->whereIn('CallId',$callid)->get();
		 			if($calls->count()):

		 				$data = [];
		 				

		 				foreach($calls as $call):
		 					$data[$call->CallId] = $call->CallDurationSeconds;
		 				endforeach;

			 			foreach($items as $item):
			 				$item->talk_sec = (isset($data[$item->uniqueid])?$data[$item->uniqueid]:0);
			 				$item->checked = $count + 1;
			 				$item->save();
			 			endforeach;		 				
		 			endif;
		 		endif;
		 		break;		 		
		endswitch;

		$logs->checker($logarr);
		return true;
	}

	public function updateLoads(){

		$Loads = new \App\Http\Models\prepaid\Load;
		$loads = $Loads->where('status',1)->get();
		
		foreach($loads as $load):
			$this->updateLoad($load);
		endforeach;

		$logs = new Logs;
		$logs->loadUpdater([
			'title' => 'load updater',
			'content' => 'load updated'
		]);
		return 1;

	}

	private function updateLoad($load){

		$Load_details = new \App\Http\Models\prepaid\Load_details;
		$details = $Load_details->where('load_id',$load->id)->get();
		$total = 0;
		foreach($details as $detail):
			$total += (roundsix($detail->talk_sec) + $detail->dispo_sec);
		endforeach;

		$min = round($total/60);
		$load->used = $min;
		$load->remaining = ($load->minutes - $min);
		if($load->remaining < 1):
			$load->status = 0;
		endif;
		$load->save();
		return 1;

	}

	public function emailer(){


		Mail::to('marc.briones@magellan-solutions')->send(new remaining());die;
		$Loads = new \App\Http\Models\prepaid\Load;
		$EmailSent = new \App\Http\Models\prepaid\EmailSent;
		$loads = $Loads->where('status',1)->get();	
		$emails = ['marc.briones@magellan-solutions.com','cleobert.delacruz@magellan-solutions.com'];

		
        Mail::send('emails.remaining', ['emails' => $emails], function ($m) use ($emails)  {
            $m->from('webdev_report@magellan-solutions.com', 'Magellan Report');
            $m->to($emails);
            $m->subject('test emailer');
        });

		
		foreach($loads as $load):
			$type = $this->type($load);

			if($type):
				if(!$EmailSent->where('load_id',$load->id)->where('type',$type)->count()):
					$Emailer = new \App\Http\Models\prepaid\Emailer;
					$emailer = $Emailer->where('subscriber_id',$load->subscriber_id)->first();
					pre($emailer->toArray());
				endif;m
			endif;

		endforeach;

	}

	private function type($load){

		$percent = round(($load->remaining/$load->minutes)*100,2);
		$result = 0 ;
		if($percent < 50):
			$result = 3;
		endif;

		if($percent < 30):
			$result = 2;
		endif;

		if($percent < 10):
			$result = 1;
		endif;

		if($percent <= 0):
			$result = 4;
		endif;
		return $result;				
	}


}