<?php namespace App\Http\Controllers\eod;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Mail\remaining;
use Auth;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use DateTimeZone;

class cronController extends Controller
{

	public function tester(){


		$this->eodStep3();
		die;
		$EodTester = new \App\Http\Models\eod\EodTester; 
		$Time = Carbon::now();
		$now = $Time->format('Y-m-d H:i');



		$Time2 = Carbon::now(new DateTimeZone('Australia/Sydney'));
		$now2 = $Time2->format('Y-m-d H:i');


		if(!$EodTester->where('triggered_time',$now)->count()):
			$r =$EodTester->create([
				'triggered_time' => $now,
				'timezone_from' => $Time->format('e'),
				'timezone_from_time' => $now,
				'timezone_to' => $Time2->format('e'),
				'timezone_to_time' => $now2				
			]);			
		endif;


	}

	public function eodStep1(){

		$Eod = new \App\Http\Models\eod\Eod;
		$Today = Carbon::now();
		$Yesterday = Carbon::yesterday();

		$dateToday = $Today->format('Y-m-d');
		$dateYesterday = $Yesterday->format('Y-m-d');

		$eods = $Eod->where('sent',1)->where('ready_to_send',1)->where('status',1)->get();

		if($eods->count()):
			foreach($eods as $e):
				if($e->timezone == 'Asia/Manila'):
					$e->converted_sendtime = $dateToday.' '.$e->send_time;

					if(strtotime($e->send_time) > strtotime($e->to)):
						$e->cto = $dateToday.' '.$e->to;
					else:
						$e->cto = $dateYesterday.' '.$e->to;
					endif;
					
					$e->cfrom = $dateToday.' '.$e->from;
					
					if(strtotime($e->cfrom) > strtotime($e->cto)):
						$e->cfrom = Carbon::createFromFormat('Y-m-d H:i:s',$e->cfrom)->subDay();
					endif;
					
				else:
					$sendtime = Carbon::createFromFormat('Y-m-d H:i:s',$dateToday.' '.$e->send_time,$e->timezone);
					$cfrom = Carbon::createFromFormat('Y-m-d H:i:s',$dateToday.' '.$e->from,$e->timezone);

					if(strtotime($e->send_time) > strtotime($e->to)):
						$cto = Carbon::createFromFormat('Y-m-d H:i:s',$dateToday.' '.$e->to,$e->timezone);
					else:
						$cto = Carbon::createFromFormat('Y-m-d H:i:s',$dateYesterday.' '.$e->to,$e->timezone);
					endif;

					$cto->setTimezone('Asia/Manila');
					$e->cto = $cto->format('Y-m-d H:i:s');

					$sendtime->setTimezone('Asia/Manila');
					$cfrom->setTimezone('Asia/Manila');
					

					$e->converted_sendtime = $sendtime->format('Y-m-d H:i:s');
					$e->cfrom = $cfrom->format('Y-m-d H:i:s');

					if(strtotime($e->cfrom) > strtotime($e->cto)):
						$e->cfrom = Carbon::createFromFormat('Y-m-d H:i:s',$e->cfrom)->subDay();
					endif;
 
				endif;
				$e->sent = 0;
				$e->ready_to_send = 0;
				$e->save();
			endforeach;
		endif;
		

	}

	public function eodStep2(){

		$now = Carbon::now()->format('Y-m-d H:i');
		$Eod = new \App\Http\Models\eod\Eod;
		$eods = $Eod->where('converted_sendtime','like',$now.'%')->where('ready_to_send',0)->where('status',1)->update(['ready_to_send' => 1]);

	}

	public function eodStep3(){

		$Eod = new \App\Http\Models\eod\Eod;
		$DboCalldetailViw = new \App\Http\Models\eod\DboCalldetailViw;
		$CampaignData = new \App\Http\Models\eod\CampaignData;
		$EodEmails = new \App\Http\Models\eod\EodEmails;

		$item = $Eod->where('ready_to_send',1)->where('sent',0)->where('status',1)->first();
		$nexus = [];
		$dboard = [];

		/*$test_email = '["marc.briones@magellan-solutions.com","cleobert.delacruz@magellan-solutions.com"]';*/
		
		if($item):

			$meta= [
				'timezone2' => $item->timezone,
				'title' => $item->name,
				'from' => $item->from,
				'from2' => $item->cfrom,
				'to' => $item->to,
				'to2' => $item->cto,
				'sender' => $item->email_from,
				'receiver' => $item->email_to,
				'cc' => $item->email_cc,
				'dboard_data' => ''
			];

			$wgroup = json_decode($item->cic_id) ? json_decode($item->cic_id,true): [] ;
			$dboardid = json_decode($item->dummyboard_id,true) ? json_decode($item->dummyboard_id,true): [] ;

			$calls = $DboCalldetailViw->whereBetween('ConnectedDate', [$item->cfrom, $item->cto])->whereIn('AssignedWorkGroup',$wgroup)->with(['dispo'])->get();
			if($calls->count()):
				$nexus = [];
				foreach($calls as $c):
					if($c->dispo):
						if(isset($nexus[$c->dispo->WrapupCode])):
							$nexus[$c->dispo->WrapupCode]++;
						else:
							$nexus[$c->dispo->WrapupCode] = 1;
						endif;
					endif;
				endforeach;
			endif;

			$meta['dialer_data'] = json_encode($nexus);

			$boarddata = $CampaignData->whereBetween('created_at', [$item->cfrom, $item->cto])->whereIn('campaign_id',$dboardid)->orderBy('campaign_id')->get();
			//pre($boarddata);
			$dboard_data = [];
			if($boarddata->count()):
				foreach($boarddata->toArray() as $a):
					$b['Form_id']  = $a['id'];
					$b['Date'] = $this->convertTime($a['created_at'],$item->timezone);
					$content = json_decode($a['contents']);
					$except = ['Request Status'];
					foreach($content as $k => $v):
						$str = $this->str_process($k);
						if(!in_array($str, $except)):
							$b[$str] = $v;
						endif;
					endforeach;
					$dboard_data[] = $b;
					unset($b);
				endforeach;
			endif;

			$meta['dboard_data'] = json_encode($dboard_data);

			$item->sent = 1;
			$item->save();
			$EodEmails->create($meta);

		endif;

	}	

	public function logTrigger($notes){
		$EodTester = new \App\Http\Models\eod\EodTester; 
		$Time = Carbon::now();
		$now = $Time->format('Y-m-d H:i');

		if(!$EodTester->where('triggered_time',$now)->count()):
			$r =$EodTester->create([
				'triggered_time' => $now,
				'notes' => $notes		
			]);			
		endif;		
	}


	public function checkTimer(){

		$EodTester = new \App\Http\Models\eod\EodTester; 
		$Time = Carbon::now();
		$now = $Time->format('Y-m-d H:i');

		if(!$EodTester->where('triggered_time',$now)->count()):
			$r =$EodTester->create([
				'triggered_time' => $now		
			]);	
			$this->convertSendTime();		
		endif;	

	}





	private function str_process($str){
		return ucwords(str_replace('_',' ',$str));
	}

	private function convertTime($date,$tz){

		$a = Carbon::createFromFormat('Y-m-d H:i:s',$date);
		if($tz != 'Asia/Manila'):
			$a->setTimezone('Australia/Sydney');
		endif;
		return $a->format('Y-m-d H:i:s');

	}

}