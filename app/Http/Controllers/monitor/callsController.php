<?php namespace App\Http\Controllers\monitor;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;


class callsController extends Controller
{

	public function calls(){

		$Logs = new \App\Http\Models\prepaid\Logs;
		$logs = $Logs->orderBy('created_at','desc')->take(10)->get();
		if($logs->count()):
			return view('monitor.tables',['items' => $logs]);
		endif;
	}
}