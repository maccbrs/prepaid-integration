<?php namespace App\Http\Controllers\monitor;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;


class updaterController extends Controller
{

	public function check($type,$min){

		$Checker_logs = new \App\Http\Models\prepaid\Checker_logs;
		$logs = false;

		switch ($min):
			case '1':
				$checktype = 0;
				break;
			case '5':
				$checktype = 1;
				break;
			case '30':
				$checktype = 2;
				break;
			case '60':
				$checktype = 3;
				break;											
		endswitch;

		if(in_array($checktype,[1,2,3,0])):
			$logs = $Checker_logs->where('type',$type)->where('checktype',$checktype)->orderBy('created_at','desc')->take(20)->get();
		endif;
		return view('monitor.tables',['items' => $logs]);

	}

}