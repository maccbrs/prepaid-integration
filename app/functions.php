<?php  


function pre($arr,$die = true){

	echo '<pre>';
	if(is_array($arr) || is_object($arr)){
		 print_r($arr);
	}else{
		echo $arr;
	}
	
	if($die) die;
}

function roundsix($n){
	if($n):
		if($n<=30) return 30;
		$a = $n - 30;
		$abs = floor($a/6)*6;
		$mod = (($n%6)>0?6:0);
		return $abs+$mod+30;
	endif;
	return 0;
}