<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',['as' => 'home','uses' => 'homeController@index']);
Route::get('/fb',['as' => 'fb','uses' => 'fbController@index']);
Route::get('/cb',['as' => 'cb','uses' => 'fbController@cb']);

Route::group(['namespace' => 'checkprepaid','prefix' => 'checkprepaid'], function () { 
	include(app_path().'/Http/Routers/checkprepaid.php'); 
}); 

Route::group(['namespace' => 'eod','prefix' => 'eod'], function () { 
	include(app_path().'/Http/Routers/eod.php'); 
}); 

Route::group(['namespace' => 'monitor','prefix' => 'monitor'], function () { 
	include(app_path().'/Http/Routers/monitor.php'); 
}); 

