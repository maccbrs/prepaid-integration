<!DOCTYPE html>
<html>
<head>
	<title></title>

	<style type="text/css">
	td, th {
		border: 1px solid black;
		text-align: left;
		font-size: 14px;
		padding: 8px;
		font-family: Calibri
	}
	p{
		font-family: Calibri;
	}
	@media only screen and (max-width: 450px) {
    .table-50 {
       	width: 100%;
    }
}
</style>
</head>
<body>
	<center>
		<p>Please note that your account might get disconnected due to insufficient minutes. To avoid disconnection, please coordinate with our Accounting department immediately.</p>
		<table width="50%" class="table-50">

			<tbody>
				<tr>
					<th bgcolor="#adebad">ACCOUNT</th>
					<td style="text-align: right; font-weight: bold;">{{ $subscriber }}</td>
				</tr>
				<tr>
					<td height="20" colspan="2"></td>
				</tr>

				<tr>
					<th bgcolor="#adebad">Total Consumed Contract Minutes %</th>
					<td style="text-align: right; font-weight: bold;">{{ $used }}</td>
				</tr>
				<tr>
					<th bgcolor="#adebad">Remaining Contract Minutes</th>
					<td style="text-align: right; font-weight: bold;">{{ $remaining }}</td>
				</tr>
				<tr>
					<td height="10" colspan="2"></td>
				</tr>
				<tr>
					<th bgcolor="#adebad">Total Consumed Overage Minutes %</th>
					<td style="text-align: right; font-weight: bold;">{{ $overage_used }}</td>
				</tr>
				<tr>
					<th bgcolor="#adebad">Remaining Overage Minutes</th>
					<td style="text-align: right; font-weight: bold;">{{ $overage_remaining }}</td>
				</tr>
				<tr>
					<td height="10" colspan="2"></td>
				</tr>
				<tr>
					<th bgcolor="#adebad">Subscription Date</th>
					<td style="text-align: right; font-weight: bold;">{{ $subscription_date }}</td>
				</tr>
				<tr>
					<th bgcolor="#adebad">Expiration Date</th>
					<td style="text-align: right; font-weight: bold;">{{ $expiration_date }}</td>
				</tr>
			</tbody>
		</table>
	</center>
	<footer>

		<p style="text-align: center">
			Developed by Magellan NOC Developers. All rights reserved.
		</p>
		<p style="text-align: center; margin-top: -15px;">
			This is a system generated email. Please do not reply. If you have concerns about this, you can send an email to <strong>teamomnitrix@magellan-solutions.com</strong>. Thank you.
		</p>

	</footer>
</body>
</html>