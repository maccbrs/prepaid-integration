<!doctype html>
<html >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <style type="text/css">
            .table-bordered > thead > tr > th, 
            .table-bordered > thead > tr > td, 
            .table-bordered > tbody > tr > th, 
            .table-bordered > tbody > tr > td, 
            .table-bordered > tfoot > tr > th, 
            .table-bordered > tfoot > tr > td{
                border: 1px solid #ddd;
                text-align: left;
                padding: 0 5px;
            }
            a{
                margin-right: 5px;
            }
        </style>


    </head>
    <body>
        <div>
        <a class="btn-sm" href="{{route('monitor.calls.index')}}">latest calls</a>|
        <a href="{{route('monitor.updater.check',['type' => 'regular','min' => 1])}}">reg checker 0</a>|
        <a href="{{route('monitor.updater.check',['type' => 'regular','min' => 5])}}">reg checker 1</a>|
        <a href="{{route('monitor.updater.check',['type' => 'regular','min' => 30])}}">reg checker 2</a>|
        <a href="{{route('monitor.updater.check',['type' => 'regular','min' => 60])}}">reg checker 3</a>|
        <a href="{{route('monitor.updater.check',['type' => 'reserved','min' => 1])}}">res checker 0</a>|
        <a href="{{route('monitor.updater.check',['type' => 'reserved','min' => 5])}}">res checker 1</a>|
        <a href="{{route('monitor.updater.check',['type' => 'reserved','min' => 30])}}">res checker 2</a>|
        <a href="{{route('monitor.updater.check',['type' => 'reserved','min' => 60])}}">res checker 3</a>        
        <hr>

        <table class="table table-bordered table-hover">
            <thead>
                <?php 
               // pre($items->toArray());
                    $keys = [];
                    foreach($items as $item):
                        foreach($item->toArray() as $key => $value):
                            if(!in_array($key,$keys)):
                                $keys[] = $key;
                            endif;
                        endforeach;
                    endforeach;
                 ?>
                <tr>
                    @if($keys)
                        @foreach($keys as $k)
                        <th>{{$k}}</th>
                        @endforeach
                    @endif
                </tr>
            </thead>
            <tbody>
                @if($items)
                    @foreach($items->toArray() as $item)
                        <tr>
                        @foreach($keys as $k)
                            <td>{{$item[$k]}}</td>
                        @endforeach
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table> 

        </div>
    </body>
</html>